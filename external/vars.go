package external

import mgo "github.com/globalsign/mgo"

var (
	//DbConn provides access to the current database session
	DbConn *mgo.Session
)
