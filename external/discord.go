package external

import (
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/spf13/viper"
	sessions "github.com/utahta/echo-sessions"

	"github.com/labstack/echo"
	"golang.org/x/oauth2"
)

var (
	discordBaseURL = "https://discordapp.com/api/"
	oauth          *oauth2.Config
)

// SetOauthConfig - Establish Oauth Configuration for Discord API
func SetOauthConfig() {
	oauth = &oauth2.Config{
		RedirectURL:  viper.GetString("web.baseURL") + ":8080/api/discord/callback",
		ClientID:     viper.GetString("discord.client_ID"),
		ClientSecret: viper.GetString("discord.client_secret"),
		Scopes:       []string{"email", "guilds", "identify"},
		Endpoint: oauth2.Endpoint{
			AuthURL:  discordBaseURL + "oauth2/authorize",
			TokenURL: discordBaseURL + "oauth2/token",
		},
	}
}

//Login provides the redirect for Discord
func Login(c echo.Context) error {
	return c.Redirect(http.StatusTemporaryRedirect, oauth.AuthCodeURL(""))
}

//IsLoggedIn checks for the existence of the Discord Auth Token and replies to the client
func IsLoggedIn(c echo.Context) error {
	_, ok := checkToken(c)

	status := []byte(`{"loggedin": ` + strconv.FormatBool(ok) + `}`)
	return c.JSONBlob(http.StatusOK, status)
}

//Callback provides the discord auth callback handler
func Callback(c echo.Context) error {
	token, err := oauth.Exchange(oauth2.NoContext, c.FormValue("code"))
	if err != nil {
		return echo.NewHTTPError(http.StatusInternalServerError, "unable to exchange auth token")
	}

	sessions.Set(c, "token", *(token))
	err = sessions.Save(c)
	if err != nil {
		c.Logger().Errorf("Session Save error: %v", err)
	}

	return c.Redirect(http.StatusPermanentRedirect, "/")
}

//GuildList provides the API endpoint for a user's guild list
func GuildList(c echo.Context) error {
	token, ok := checkToken(c)
	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, "no token found")
	}

	resp, err := apiCall("users/@me/guilds", "GET", token.AccessToken)
	if err != nil || resp.StatusCode == http.StatusNotFound {
		c.Logger().Errorf("error in discord api: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Cannot retrieve information")
	}
	defer resp.Body.Close()

	guildList, _ := ioutil.ReadAll(resp.Body)
	return c.JSONBlob(http.StatusOK, guildList)
}

//ProfileInfo - Creates an API call to Discord to obtain profile information
func ProfileInfo(c echo.Context) error {
	token, ok := checkToken(c)
	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, "no token found")
	}
	resp, err := apiCall("users/@me", "GET", token.AccessToken)
	if err != nil || resp.StatusCode == http.StatusNotFound {
		c.Logger().Errorf("error in discord api: %v", err)
		return echo.NewHTTPError(http.StatusInternalServerError, "Cannot retrieve information")
	}
	defer resp.Body.Close()

	profileInfo, _ := ioutil.ReadAll(resp.Body)
	return c.JSONBlob(http.StatusOK, profileInfo)
}

//Logout - Removes the oauth token from a User Session
func Logout(c echo.Context) error {
	_, ok := checkToken(c)
	if !ok {
		return echo.NewHTTPError(http.StatusForbidden, "no token found")
	}
	sessions.Clear(c)
	sessions.Save(c)
	return c.String(http.StatusOK, "token deleted")
}

func apiCall(endpoint, method, token string) (*http.Response, error) {
	req, err := http.NewRequest(method, discordBaseURL+endpoint, nil)
	req.Header.Set("Accept", "application/json")
	req.Header.Set("Authorization", "Bearer "+token)
	cl := &http.Client{}
	response, err := cl.Do(req)
	return response, err
}

func checkToken(c echo.Context) (oauth2.Token, bool) {
	if t := sessions.GetRaw(c, "token"); t == nil {
		return oauth2.Token{}, false
	}
	return sessions.GetRaw(c, "token").(oauth2.Token), true
}
