package main

import (
	"github.com/labstack/echo"
	"gitlab.com/ucgc/webServer/external"
)

func setRoutes(e *echo.Echo) {
	external.SetOauthConfig()
	e.GET("/favicon.ico", echo.NotFoundHandler)

	d := e.Group("api/discord/")
	d.GET("login", external.Login)
	d.GET("callback", external.Callback)
	d.GET("guilds", external.GuildList)
	d.GET("profile", external.ProfileInfo)
	d.GET("logout", external.Logout)
}

func index(c echo.Context) error {
	return nil
}
