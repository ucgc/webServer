package main

import (
	"encoding/gob"
	"os"
	"os/signal"

	"gitlab.com/ucgc/webServer/external"

	"github.com/boj/redistore"
	mgo "github.com/globalsign/mgo"
	sessions "github.com/utahta/echo-sessions"
	"golang.org/x/oauth2"

	"github.com/spf13/viper"

	"github.com/Sirupsen/logrus"

	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
)

func init() {
	viper.SetConfigName("config")
	viper.AddConfigPath(".")
	if err := viper.ReadInConfig(); err != nil {
		logrus.Fatalf("Cannot read config file. Did you copy the example?\n %v", err)
	}
}

func main() {
	e := echo.New()
	e.HideBanner = true //Hiding the banner on startup

	e.Use(middleware.Recover()) // Use the Recover middleware to handle panics and fatals within an http chain
	store, err := redistore.NewRediStore(10, "tcp",
		viper.GetString("web.redis.host"),
		viper.GetString("web.redis.pwd"),
		[]byte(viper.GetString("web.storageKey")))
	if err != nil {
		logrus.Fatalf("issue getting redistore: %v", err)
	}

	e.Use(sessions.Sessions("SESSID", store))
	e.Use(middleware.Logger())

	e.File("/", "public/index.html")
	e.Static("/static", "public/static")

	//Register Token type to allow storage in sessions
	gob.Register(oauth2.Token{})

	setDatabase()
	setRoutes(e)

	go func() {
		logrus.Fatalf("Server could not start: %v", e.Start(":8080"))
	}()

	sig := make(chan os.Signal)
	signal.Notify(sig, os.Interrupt, os.Kill)
	if s := <-sig; s != nil {
		logrus.Infof("Recieved Signal: %v\nShutting down server...\n", s)
	}
}

func setDatabase() {
	mgo, err := mgo.Dial(viper.GetString("web.mongo.host"))
	err = mgo.DB("web").Login(viper.GetString("web.mongo.user"), viper.GetString("web.mongo.pwd"))
	if err != nil {
		logrus.Errorf("Mongo error: %v", err)
	}

	external.DbConn = mgo
}
